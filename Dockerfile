FROM ubuntu:latest

# Install necessary packages
RUN apt-get update && \
    apt-get install -y gcc sudo

# Create a new user and set it as the working user
RUN useradd -ms /bin/bash myuser

# Set the working directory
WORKDIR /home/myuser

# Copy the source code
COPY ../database/database.c ./database/
COPY ../client/client.c ./client/

# Compile the programs
RUN gcc -pthread ./database/database.c -o ./database/database
RUN gcc ./client/client.c -o ./client/client

# Set the entry point to run the database and client programs
CMD ["bash", "-c", "./database/database & sleep 7 && ./client/client"]
