#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h> 
#include <dirent.h>
#include <syslog.h>
#define PORT 8080
#define BUFF 1024
#define SIZE 100

int valread;
char *users_file = "users.txt";
char *database = "problems.txt";
char *db_folder="./databases/";
char *user_db = "./databases/user.db";
char userlog[SIZE]={};
int server_fd;
struct sockaddr_in address;
int opt = 1, new_socket;
int addrlen = sizeof(address);

char folderDB[] = "/home/farah/Documents/database/databases/";
char fileUser[] = "/home/farah/Documents/database/databases/user.db";
char currentDB[BUFF] = {0};
int isRoot = 0;
int acc = 0;

struct login {
    char id[BUFF];
    char password[BUFF];
} login;

void logging(char *command, char *uname){
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
 
	char line[1000];

	FILE *file;
	file = fopen("/home/farah/Documents/database/databases/user_query.log", "ab");

	sprintf(line, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n",timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, uname, command);
	// sprintf(line, "%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, uname, filepath);

	fputs(line, file);
	fclose(file);
	return;

}

void makeFolder(char fdname[])
{
    char* dirname;
    dirname = malloc(strlen(fdname) + 1);
    strcpy(dirname, fdname);
    mkdir(dirname,0777);
}

void createFile(char fileName[], char str[], char mode[])
{
    FILE* file = fopen(fileName, mode);
    fprintf(file, "%s\n", str);
    fclose(file);
}

int auth(char str[])
{
    FILE* file = fopen(fileUser, "r+");

    char line[BUFF];
    while (fgets(line, BUFF, file))
    {
        if (!strcmp(line, str))
        {
            fclose(file);
            return 1;
        }
    }
    fclose(file);

    return 0;
}

void reconnect()
{
    char buffer[BUFF] = {0}, msg[BUFF] = {0};
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    // ambil uid
    valread = read(new_socket, buffer, BUFF);

    // root user
    if (!strcmp(buffer, "0"))
    {
        strcpy(msg, "Connection Accepted. Connected As Root!");
        strcpy(login.id, "root");
        acc = 1;
        isRoot = 1;
    }

    if (!acc)
    {
        // handle read di client
        send(new_socket, "TESTT", 10, 0);
        //bzero(buffer, BUFF);
        memset(buffer, 0, BUFF);

        valread = read(new_socket, buffer, BUFF);

        if (auth(buffer))
        {
            char strbackup[BUFF];
            strcpy(strbackup, buffer);
            char* ptr = strbackup;
            char* token;

            int i;
            for (i = 0; token = strtok_r(ptr, ":", &ptr); i++)
            {
                if (i == 0)
                {
                    strcpy(login.id, token);
                }
                else if (i == 1)
                {
                    strcpy(login.password, token);
                }
            }
            acc = 1;
            strcpy(msg, "Connection Accepted. Welcome ");
			strcat(msg, login.id);
        }
        else
        {
            strcpy(msg, "Invalid Username or Password!");
        }
    }
    send(new_socket, msg, strlen(msg), 0);
}

char* createUser(char str[])
{
    char* ptr;
    char msg[BUFF];
    memset(msg, 0, BUFF);

    char newuserid[BUFF];
    memset(newuserid, 0, BUFF);
    char newuserpass[BUFF];
    memset(newuserpass, 0, BUFF);

    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;
    int i;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++)
    {
        if (i == 2)
        {
            strcpy(newuserid, token);
        }
        else if (i == 5)
        {
            strncpy(newuserpass, token, strlen(token) - 1);
        }
        else if ((i == 3 && strcmp(token, "IDENTIFIED")) || (i == 4 && strcmp(token, "BY")) || i > 5)
        {
			strcpy(msg, "SYNTAX ERROR!");
            ptr = msg;
            return ptr;
        }
    }

    memset(cmd, 0, BUFF);
	strcpy(cmd, newuserid); strcat(cmd, ":"); strcat(cmd, newuserpass);
    createFile(fileUser, cmd, "a+");
    strcpy(msg, "CREATE USER SUCCESS!");

    logging(str, login.id);
    ptr = msg;
    return ptr;
}

char* use(char str[])
{
    char* ptr;
    char msg[BUFF];
    memset(msg, 0, BUFF);

    char* dbptr = str + 4;
    char dbName[BUFF];
    memset(dbName, 0, BUFF);
    strncpy(dbName, dbptr, strlen(dbptr) - 1);

    char permissionFile[BUFF];
    memset(permissionFile, 0, BUFF);
    sprintf(permissionFile, "%s%s/grantedUser.txt", folderDB, dbName);

    FILE* file = fopen(permissionFile, "r");
    if (!file)
    {
        strcpy(msg, "DATABASE NOT FOUND");
		ptr = msg;
		return ptr;
    }

    char line[BUFF];
    while (fgets(line, BUFF, file))
    {
        if (!strncmp(line, login.id, strlen(login.id)))
        {
            fclose(file);
            strcpy(currentDB, dbName);
            strcpy(msg, "DATABASE CHANGE TO ");
            strcat(msg, currentDB);
            ptr = msg;
            logging(str, login.id);
            return ptr;
        }
    }
    fclose(file);
    strcpy(msg, "ACCESS DATABASE DENIED");
    ptr = msg;
    return ptr;
}

char* grantPermission(char str[])
{
    char* ptr;
    char msg[BUFF];
    memset(msg, 0, BUFF);

    char dbName[BUFF];
    memset(dbName, 0, BUFF);

    char userid[BUFF];
    memset(userid, 0, BUFF);

    int i;
    char parse[BUFF];
    strcpy(parse, str);
    char* parseptr = parse;
    char* token;

    for (i = 0; token = strtok_r(parseptr, " ", &parseptr); i++)
    {
        if (i == 2)
        {
            strcpy(dbName, token);
        }
        else if (i == 4)
        {
            strncpy(userid, token, strlen(token) - 1);
        }
        else if (i == 3 && strcmp(token, "INTO"))
        {
            strcpy(msg, "SYNTAX ERROR!");
            ptr = msg;
            return ptr;
        }
    }

    FILE* file = fopen(fileUser, "r");
    char line[BUFF];
    int ada = 0;

    while (fgets(line, BUFF, file))
    {
        if (!strncmp(line, userid, strlen(userid)))
        {
            ada = 1;
            break;
        }
    }
    fclose(file);

    if (!ada)
    {
        strcpy(msg, "USER NOT FOUND!");
        ptr = msg;
        return ptr;
    }

    char namafile[BUFF];
    sprintf(namafile, "%s%s/grantedUser.txt", folderDB, dbName);
    
    file = fopen(namafile, "a+");
    if (!file)
    {
        strcpy(msg, "DATABASE NOT FOUND");
        ptr = msg;
        return ptr;
    }
    fprintf(file, "%s\n", userid);
    fclose(file);

    strcpy(msg, "ACCESS GRANTED");
    ptr = msg;
    logging(str, login.id);
    return ptr;
}

char* createDB (char str[]){
	char* ptr;
	char msg[BUFF];

	char dbName[BUFF];
	//bzero(dbName, BUFF);
    memset(dbName, 0, BUFF);

	int i;
	char parse[BUFF];
	strcpy(parse, str);
	char* parseptr = parse;
	char* token;

	for (i = 0; token = strtok_r(parseptr, " ", &parseptr); i++){
		if (i == 2)
		{
			strncpy(dbName, token, strlen(token) - 1);
		}
	}

	char dbPath[BUFF];
	sprintf(dbPath, "%s%s", folderDB, dbName);
	char* pathdbptr = dbPath;

	if (mkdir(pathdbptr, 0777) != 0) {
		strcpy(msg, "CANNOT CREATE DATABASE!");
		ptr = msg;
		return ptr;
	}

	char permissionFile[BUFF];
	strcpy(permissionFile, dbPath);
	strcat(permissionFile, "/grantedUser.txt");

	createFile(permissionFile, login.id, "a+");

	strcpy(msg, "DATABASE SUCCESSFULLY CREATED!");
	ptr = msg;
    logging(str, login.id);
	return ptr;
}

int removeDir(const char *path) {
	DIR* dir = opendir(path);
	size_t path_len = strlen(path);

    int result = -1;


	if (!dir){
        return -1;
    }

   	if (dir) {

        struct dirent *entry;


        result = 0;
        while((entry = readdir(dir)) != NULL) {

            
            int status = -1;
            char entry_path[256];
            
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
				continue;
            }


            snprintf(entry_path, sizeof(entry_path), "%s/%s", path, entry->d_name);
            struct stat entry_stat;
            if (stat(entry_path, &entry_stat) == 0) {
                if (S_ISDIR(entry_stat.st_mode)) {
                    status = removeDir(entry_path);
                } else {
                    status = unlink(entry_path);
                }
            }
            if (status == -1) {
                result = -1;  // Gagal menghapus direktori/file
                break;
            }
		}
		// closedir(d);
        closedir(dir);
	}


    if (result != -1 && rmdir(path) == -1) {
        result = -1;  // Gagal menghapus direktori itu sendiri
    }

	return result;
}


char* dropDB(char str[])
{

	char* ptr;
	char msg[BUFF];

	char dbName[BUFF];

    memset(dbName, 0, BUFF);

	int i;
	char parse[BUFF];
	strcpy(parse, str);
	char* parseptr = parse;
	char* token;

	for (i = 0; token = strtok_r(parseptr, " ", &parseptr); i++)
	{
		if (i == 2)
		{
			strncpy(dbName, token, strlen(token) - 1);
		}
	}

	char dbPath[BUFF];
	sprintf(dbPath, "%s%s", folderDB, dbName);
	char* pathdbptr = dbPath;

    DIR* dir = opendir(dbPath);

	if (!dir) {
		strcpy(msg, "DATABASE NOT EXIST!");
		ptr = msg;
		return ptr;
	}
    closedir(dir);

	char permissionFile[BUFF];
    sprintf(permissionFile, "%s/%s", dbPath, "grantedUser.txt");


	FILE* file = fopen(permissionFile, "r");

    if (!file) {
        strcpy(msg, "USER HAS NO PERMISSION!");
        ptr = msg;
        return ptr;
    }

    char line[BUFF];
    memset(line, 0, BUFF);
    int hasPermission = 0;

	while (fgets(line, BUFF, file))
	{
        if (strstr(line, login.id) != NULL)
		{
            hasPermission = 1;
			break;
		}
	}

    fclose(file);

    if (hasPermission) {
        if (removeDir(dbPath) == 0) {
            strcpy(msg, "DATABASE DROPPED.");
            ptr = msg;
            logging(str, login.id);
            return ptr;
        }
        else {
            strcpy(msg, "FAILED TO DROP DATABASE.");
            ptr = msg;
            return ptr;
        }
    }
	strcpy(msg, "USER HAS NO PERMISSION!");
	ptr = msg;
    logging(str, login.id);
	return ptr;
}

char* createTable(char str[]) {

	char* ptr;
	char msg[BUFF];

	char tableName[BUFF];
    memset(tableName, 0, BUFF);
	
	int i = 0;
	char cmd[BUFF];
	strcpy(cmd, str);
	char* cmdptr = cmd;
	char* token;

	for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++)
	{
		if (i == 2)
		{
			strcpy(tableName, token);
		}
	}

    char* bracketPtr = strchr(str, '(');
    if (!bracketPtr)
    {
        strcpy(msg, "INVALID COMMAND.");
        ptr = msg;
        return ptr;
    }

    char* cmdptr1 = bracketPtr + 1;
    strncpy(cmd, cmdptr1, strlen(cmdptr1) - 2);
    char temp[BUFF];
    strcpy(temp, cmd);
    
	char tablePath[BUFF];
	sprintf(tablePath, "%s%s/%s.txt", folderDB, currentDB, tableName);

	FILE* file = fopen(tablePath, "w+");
    if (!file) {
        strcpy(msg, "FAILED TO CREATE TABLE.");
        ptr = msg;
        return ptr;
    }   


	char* token2;
	int j = 0;
	
	char columnName[BUFF], columnData[BUFF];
	for (i = 0; token = strtok_r(temp, ",", &temp); i++)
	{
		char column[BUFF];
		strcpy(column, token);
        char* columnPtr = column;
		for (j = 0; token2 = strtok_r(columnPtr, " ", &columnPtr); j++)
		{
			if (j == 0)
			{
				strcpy(columnName, token2);
			}
			if (j == 1)
			{
				strcpy(columnData, token2);
			}
		}
        fprintf(file, "%s:%s", columnName, columnData);
        fprintf(file, "\t");
	}
	fclose(file);

	strcpy(msg, "TABLE CREATED.");
	ptr = msg;
    logging(str, login.id);
	return ptr;
}

char* dropTable(char str[]){
    char* ptr;
	char msg[BUFF];
 
	char tableName[BUFF];
    memset(tableName, 0, BUFF);
 
	// temp
	int i = 0;
	char cmd[BUFF];
	strcpy(cmd, str);
	char* cmdptr = cmd;
	char* token;
 
	// DROP TABLE 
	for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++)
	{
		if (i == 2)
		{
			strncpy(tableName, token, strlen(token) - 1);
		}
	}
 
	char tablePath[BUFF];
	sprintf(tablePath, "%s%s/%s.txt", folderDB, currentDB, tableName);
 
	FILE* file = fopen(tablePath, "r");
    if (!file)
    {
        strcpy(msg, "TABLE NOT FOUND.");
        ptr = msg;
        return ptr;
    }
    fclose(file);
 
	int status;
	pid_t child_id = fork();
  
    if (child_id < 0) {
        strcpy(msg, "FAILED TO DROP TABLE.");
        ptr = msg;
        return ptr;
    } else if (child_id == 0)
    {
        // Child process
        char* argv[] = {"rm", tablePath, NULL};
        execv("/bin/rm", argv);
        exit(EXIT_FAILURE);
    } else {
        // Parent process
        wait(&status);
        if (WEXITSTATUS(status) == 0)
        {
            strcpy(msg, "TABLE DROPPED.");
            ptr = msg;
            logging(str, login.id);
            return ptr;
        }
        else
        {
            strcpy(msg, "FAILED TO DROP TABLE.");
            ptr = msg;
            return ptr;
        }
    }   
	
}

char* insert_table(char str[]) {
    if (strlen(currentDB) == 0) {
        printf("NO DATABASE SELECTED.\n");
        return;
    }

    char tableName[BUFF];
    char values[BUFF];
    memset(tableName, 0, BUFF);
    memset(values, 0, BUFF);

    int i = 0;
    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++) {
        if (i == 1 && strcmp(token, "INTO") != 0) {
            printf("INVALID COMMAND. Missing 'INTO'.\n");
            return;
        }
        if (i == 2) {
            strcpy(tableName, token);
        }
        if (i == 3 && strcmp(token, "(") != 0) {
            printf("INVALID COMMAND. Missing '('.\n");
            return;
        }
        if (i > 3 && strcmp(token, ")") == 0) {
            break;
        }
        if (i > 3) {
            strcat(values, token);
            strcat(values, " ");
        }
    }

    if (strlen(tableName) == 0 || strlen(values) == 0) {
        printf("SYNTAX ERROR!\n");
        return;
    }

    // Menghilangkan spasi ekstra di akhir daftar nilai
    values[strlen(values) - 1] = '\0';

    char filename[BUFF];
    sprintf(filename, "%s%s/%s.txt", folderDB, currentDB, tableName);

    FILE* file = fopen(filename, "a");
    if (!file) {
        printf("FAILED TO OPEN TABLE.\n");
        return;
    }

    fprintf(file, "%s\n", values);
    fclose(file);

    printf("DATA INSERTED.\n");
}


char* select_table(char str[]) {
    if (strlen(currentDB) == 0) {
        printf("NO DATABASE SELECTED.\n");
        return;
    }

    char tableName[BUFF];
    char columnName[BUFF];
    char value[BUFF];
    memset(tableName, 0, BUFF);
    memset(columnName, 0, BUFF);
    memset(value, 0, BUFF);

    int i = 0;
    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;

    // Menggunakan strtok_r untuk mendapatkan token yang diinginkan
    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++) {
        if (i == 3 && strcmp(token, "*") == 0) {
            token = strtok_r(NULL, " ", &cmdptr);  // Memeriksa kata "FROM"
            if (token && strcmp(token, "FROM") == 0) {
                token = strtok_r(NULL, " ", &cmdptr);  // Mendapatkan nama tabel
                if (token) {
                    strcpy(tableName, token);
                    break;
                }
            }
        }
    }

    if (strlen(tableName) == 0) {
        printf("INVALID COMMAND.\n");
        return;
    }

    token = strtok_r(NULL, " ", &cmdptr);  // Memeriksa kata "WHERE"
    if (token && strcmp(token, "WHERE") == 0) {
        token = strtok_r(NULL, "=", &cmdptr);  // Mendapatkan nama kolom
        if (token) {
            strcpy(columnName, token);
            token = strtok_r(NULL, " ", &cmdptr);  // Memeriksa operator "="
            if (token && strcmp(token, "=") == 0) {
                token = strtok_r(NULL, " ", &cmdptr);  // Mendapatkan value
                if (token) {
                    strcpy(value, token);
                }
            }
        }
    }

    char filename[BUFF];
    sprintf(filename, "%s%s/%s.txt", folderDB, currentDB, tableName);

    FILE* file = fopen(filename, "r");
    if (!file) {
        printf("TABLE NOT FOUND.\n");
        return;
    }

    char line[BUFF];
    while (fgets(line, BUFF, file)) {
        // Jika kolom dan value tidak kosong, lakukan filtering berdasarkan nilai kolom
        if (strlen(columnName) > 0 && strlen(value) > 0) {
            char* columnValue = strtok(line, ",");
            if (columnValue && strcmp(columnValue, columnName) == 0) {
                columnValue = strtok(NULL, ",");
                if (columnValue && strcmp(columnValue, value) == 0) {
                    printf("%s", line);
                    printf("DATA SELECTED.\n");
                }
            }
        }
        // Jika kolom dan value kosong, cetak semua baris
        else {
            printf("%s", line);
            printf("DATA SELECTED.\n");
        }
    }

    fclose(file);
}

char* update_table(char str[]) {
    if (strlen(currentDB) == 0) {
        printf("NO DATABASE SELECTED.\n");
        return;
    }

    char tableName[BUFF];
    char columnName[BUFF];
    char columnValue[BUFF];
    char conditionColumn[BUFF];
    char conditionValue[BUFF];
    memset(tableName, 0, BUFF);
    memset(columnName, 0, BUFF);
    memset(columnValue, 0, BUFF);
    memset(conditionColumn, 0, BUFF);
    memset(conditionValue, 0, BUFF);

    int i = 0;
    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++) {
        if (i == 1) {
            strcpy(tableName, token);
        } else if (i == 3) {
            if (strcmp(token, "SET") != 0) {
                printf("INVALID COMMAND. Missing 'SET'.\n");
                return;
            }
        } else if (i % 4 == 0) {
            if (strcmp(token, "WHERE") == 0) {
                break;
            } else {
                strcpy(columnName, token);
            }
        } else if (i % 4 == 2) {
            strcpy(columnValue, token);
        } else if (i % 4 == 3) {
            if (strcmp(token, "WHERE") != 0) {
                printf("INVALID COMMAND. Missing 'WHERE'.\n");
                return;
            }
        } else if (i % 4 == 1) {
            strcpy(conditionColumn, token);
        } else if (i % 4 == 3) {
            strcpy(conditionValue, token);
        }
    }

    if (strlen(tableName) == 0 || strlen(columnName) == 0 || strlen(columnValue) == 0 ||
        strlen(conditionColumn) == 0 || strlen(conditionValue) == 0) {
        printf("INVALID COMMAND.\n");
        return;
    }

    char filename[BUFF];
    sprintf(filename, "%s%s/%s.txt", folderDB, currentDB, tableName);

    FILE* file = fopen(filename, "r");
    if (!file) {
        printf("TABLE NOT FOUND.\n");
        return;
    }

    char tempFilename[BUFF];
    sprintf(tempFilename, "%s%s/%s_temp.txt", folderDB, currentDB, tableName);

    FILE* tempFile = fopen(tempFilename, "w");
    if (!tempFile) {
        printf("FAILED TO OPEN TEMPORARY FILE.\n");
        fclose(file);
        return;
    }

    char line[BUFF];
    int updated = 0;
    while (fgets(line, BUFF, file)) {
        char tempLine[BUFF];
        strcpy(tempLine, line);
        char* token = strtok(tempLine, ",");
        int columnFound = 0;
        while (token != NULL) {
            char tempColumnValue[BUFF];
            strcpy(tempColumnValue, token);
            if (strcmp(token, columnName) == 0) {
                columnFound = 1;
                strcpy(tempColumnValue, columnValue);
            }
            fprintf(tempFile, "%s,", tempColumnValue);
            token = strtok(NULL, ",");
        }
        if (columnFound) {
            updated = 1;
        }
        fprintf(tempFile, "\n");
    }

    fclose(file);
    fclose(tempFile);

    if (!updated) {
        printf("NO MATCHING RECORDS FOUND.\n");
        remove(tempFilename);
        return;
    }

    remove(filename);
    rename(tempFilename, filename);

    printf("DATA UPDATED.\n");
}

char* delete_from_table(char str[]) {
    if (strlen(currentDB) == 0) {
        printf("NO DATABASE SELECTED.\n");
        return;
    }

    char tableName[BUFF];
    char conditionColumn[BUFF];
    char conditionValue[BUFF];
    memset(tableName, 0, BUFF);
    memset(conditionColumn, 0, BUFF);
    memset(conditionValue, 0, BUFF);

    int i = 0;
    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++) {
        if (i == 2) {
            strcpy(tableName, token);
        } else if (i == 4) {
            if (strcmp(token, "WHERE") != 0) {
                printf("INVALID COMMAND. Missing 'WHERE'.\n");
                return;
            }
        } else if (i % 4 == 1) {
            strcpy(conditionColumn, token);
        } else if (i % 4 == 3) {
            strcpy(conditionValue, token);
        }
    }

    if (strlen(tableName) == 0 || strlen(conditionColumn) == 0 || strlen(conditionValue) == 0) {
        printf("SYNTAX ERROR!\n");
        return;
    }

    char filename[BUFF];
    sprintf(filename, "%s%s/%s.txt", folderDB, currentDB, tableName);

    FILE* file = fopen(filename, "r");
    if (!file) {
        printf("TABLE NOT FOUND.\n");
        return;
    }

    char tempFilename[BUFF];
    sprintf(tempFilename, "%s%s/%s_temp.txt", folderDB, currentDB, tableName);

    FILE* tempFile = fopen(tempFilename, "w");
    if (!tempFile) {
        printf("FAILED TO OPEN TEMPORARY FILE.\n");
        fclose(file);
        return;
    }

    char line[BUFF];
    int deleted = 0;
    while (fgets(line, BUFF, file)) {
        char tempLine[BUFF];
        strcpy(tempLine, line);
        char* token = strtok(tempLine, ",");
        int columnFound = 0;
        while (token != NULL) {
            char tempColumnValue[BUFF];
            strcpy(tempColumnValue, token);
            if (strcmp(token, conditionColumn) == 0) {
                if (strcmp(conditionValue, tempColumnValue) == 0) {
                    columnFound = 1;
                    break;
                }
            }
            token = strtok(NULL, ",");
        }
        if (!columnFound) {
            fprintf(tempFile, "%s", line);
        } else {
            deleted = 1;
        }
    }

    fclose(file);
    fclose(tempFile);

    if (!deleted) {
        printf("NO MATCHING RECORDS FOUND.\n");
        remove(tempFilename);
        return;
    }

    remove(filename);
    rename(tempFilename, filename);

    printf("DATA DELETED.\n");
}

char* drop_column(char str[]) {
    if (strlen(currentDB) == 0) {
        printf("NO DATABASE SELECTED.\n");
        return;
    }

    char columnName[BUFF];
    char tableName[BUFF];
    memset(columnName, 0, BUFF);
    memset(tableName, 0, BUFF);

    int i = 0;
    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++) {
        if (i == 2) {
            strcpy(columnName, token);
        } else if (i == 4) {
            if (strcmp(token, "FROM") != 0) {
                printf("INVALID COMMAND. Missing 'FROM'.\n");
                return;
            }
        } else if (i == 5) {
            strcpy(tableName, token);
            break;
        }
    }

    if (strlen(columnName) == 0 || strlen(tableName) == 0) {
        printf("SYNTAX ERROR!\n");
        return;
    }

    char filename[BUFF];
    sprintf(filename, "%s%s/%s.txt", folderDB, currentDB, tableName);

    FILE* file = fopen(filename, "r");
    if (!file) {
        printf("TABLE NOT FOUND.\n");
        return;
    }

    char tempFilename[BUFF];
    sprintf(tempFilename, "%s%s/%s_temp.txt", folderDB, currentDB, tableName);

    FILE* tempFile = fopen(tempFilename, "w");
    if (!tempFile) {
        printf("FAILED TO OPEN TEMPORARY FILE.\n");
        fclose(file);
        return;
    }

    char line[BUFF];
    int columnDeleted = 0;
    while (fgets(line, BUFF, file)) {
        char tempLine[BUFF];
        strcpy(tempLine, line);

        char* token = strtok(tempLine, ",");
        int columnCount = 0;
        while (token != NULL) {
            char tempColumnName[BUFF];
            strcpy(tempColumnName, token);
            if (strcmp(columnName, tempColumnName) == 0) {
                columnDeleted = 1;
            } else {
                if (columnCount > 0) {
                    fprintf(tempFile, ",");
                }
                fprintf(tempFile, "%s", token);
                columnCount++;
            }
            token = strtok(NULL, ",");
        }

        if (columnCount > 0) {
            fprintf(tempFile, "\n");
        }
    }

    fclose(file);
    fclose(tempFile);

    if (!columnDeleted) {
        printf("COLUMN NOT FOUND.\n");
        remove(tempFilename);
        return;
    }

    remove(filename);
    rename(tempFilename, filename);

    printf("COLUMN DROPPED.\n");
}


int main(int argc, char const *argv[]) {
    
    makeFolder("databases");

    pid_t pid, sid;
    pid = fork();

    if (pid < 0) {
            exit(EXIT_FAILURE);
    }
    
    if (pid > 0) {
            exit(EXIT_SUCCESS);
    }
    
    umask(0);

    sid = setsid();
    if (sid < 0) {
            exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }
    

    char buffer[BUFF] = {0}, msg[BUFF] = {};
    
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }
    
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    address.sin_port = htons( PORT );
    
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))< 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }

    int *new_sock = malloc(1);
	*new_sock = new_socket;
    
    reconnect();
       
    while (1) {
        //bzero(buffer, BUFF);
        memset(buffer, 0, BUFF);
        valread = read(new_socket, buffer, BUFF);
        
        if (!valread)
        {
            acc = 0;
            isRoot = 0;
            reconnect();
            continue;
        }

        if (buffer[strlen(buffer) - 1] != ';')
        {
            strcpy(msg, "SYNTAX ERROR!");
        }
        else if (!strncmp(buffer, "CREATE USER", 11))
        {
            if (isRoot)
            {
                strcpy(msg, createUser(buffer));
            }
            else
            {
                strcpy(msg, "USER HAS NO PERMISSION");
            }
        }
        
        else if (!strncmp(buffer, "GRANT PERMISSION", 16))
        {
            if (isRoot)
            {
                strcpy(msg, grantPermission(buffer));
            }
            else
            {
                strcpy(msg, "USER HAS NO PERMISSION");
            }
        }
        else if (!strncmp(buffer, "USE", 3))
        {
            strcpy(msg, use(buffer));
        }
		else if (!strncmp(buffer, "CREATE DATABASE", 15))
		{
			strcpy(msg, createDB(buffer));
		}
		else if (!strncmp(buffer, "DROP DATABASE", 13))
		{
			strcpy(msg, dropDB(buffer));
		}
		else if (!strncmp(buffer, "CREATE TABLE", 12))
		{
			strcpy(msg, createTable(buffer));
		}
        else if (!strncmp(buffer, "DROP TABLE", 10))
		{
			strcpy(msg, dropTable(buffer));
		}
        else if (!strncmp(buffer, "INSERT INTO", 11))
        {
            strcpy(msg, insert_table(buffer));

        }
        else if (!strncmp(buffer, "SELECT", 6)) 
        {
            strcpy(msg, select_table(buffer));
        }
        else if (!strncmp(buffer, "UPDATE", 6)) 
        {
            strcpy(msg, update_table(buffer));
        }
        else if (!strncmp(buffer, "DELETE FROM", 11)) 
        {
            strcpy(msg, delete_from_table(buffer));
        }
        else if (!strncmp(buffer, "DROP COLUMN", 11)) 
        {
            strcpy(msg, drop_column(buffer));
        }
        else
        {
            strcpy(msg, "QUERY NOT AVAILABLE!");
        }
        send(new_socket, msg, strlen(msg), 0);
    }

    return 0;
}
