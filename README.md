# sisop-praktikum-fp-2023-BS-D05

Anggota Kelompok 

|   Nama               | NRP      |
| ------               | ------   |
|  Farah Dhia Fadhila  |5025211030|
|  Najma Ulya Agustina |5025211239|
|  Rayhan Almer Kusumah|5025211115|

****DATABASE****

**A. Autentikasi**

**---User (bukan root) dapat mengakses program client---**

	Syntax:

> ./[program_client_database] -u [username] -p [password]

- output:

![login](dokumentasi/login success.png)

**---User root mengakses program client---**

	Syntax:

> sudo ./client_database

- Output:

![loginroot](dokumentasi/create table.jpg)


**---Menambahkan user (Hanya bisa dilakukan user root)---**

	Syntax:

> CREATE USER [nama_user] IDENTIFIED BY [password_user];

- Program:

`char* createUser(char str[])
{
    char* ptr;
    char msg[BUFF];
    memset(msg, 0, BUFF);

    char newuserid[BUFF];
    memset(newuserid, 0, BUFF);
    char newuserpass[BUFF];
    memset(newuserpass, 0, BUFF);

    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;
    int i;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++)
    {
        if (i == 2)
        {
            strcpy(newuserid, token);
        }
        else if (i == 5)
        {
            strncpy(newuserpass, token, strlen(token) - 1);
        }
        else if ((i == 3 && strcmp(token, "IDENTIFIED")) || (i == 4 && strcmp(token, "BY")) || i > 5)
        {
			strcpy(msg, "SYNTAX ERROR!");
            ptr = msg;
            return ptr;
        }
    }

    memset(cmd, 0, BUFF);
	strcpy(cmd, newuserid); strcat(cmd, ":"); strcat(cmd, newuserpass);
    createFile(fileUser, cmd, "a+");
    strcpy(msg, "CREATE USER SUCCESS!");

    logging(str, login.id);
    ptr = msg;
    return ptr;
}
`


- Output:

![user](dokumentasi/grant.png)


**B. Autorisasi**

**---Melakukan Akses Database---**

	Syntax:
> USE [nama_database];

- Program: 

`char* use(char str[])
{
    char* ptr;
    char msg[BUFF];
    memset(msg, 0, BUFF);

    char* dbptr = str + 4;
    char dbName[BUFF];
    memset(dbName, 0, BUFF);
    strncpy(dbName, dbptr, strlen(dbptr) - 1);

    char permissionFile[BUFF];
    memset(permissionFile, 0, BUFF);
    sprintf(permissionFile, "%s%s/grantedUser.txt", folderDB, dbName);

    FILE* file = fopen(permissionFile, "r");
    if (!file)
    {
        strcpy(msg, "DATABASE NOT FOUND");
		ptr = msg;
		return ptr;
    }

    char line[BUFF];
    while (fgets(line, BUFF, file))
    {
        if (!strncmp(line, login.id, strlen(login.id)))
        {
            fclose(file);
            strcpy(currentDB, dbName);
            strcpy(msg, "DATABASE CHANGE TO");
            strcat(msg, currentDB);
            ptr = msg;
            logging(str, login.id);
            return ptr;
        }
    }
    fclose(file);
    strcpy(msg, "ACCESS DATABASE DENIED");
    ptr = msg;
    return ptr;
}
`
- Output:

![use](dokumentasi/create table.jpg)

**---Memberikan permission atas database untuk suatu user (hanya root)---**

	Syntax:
> GRANT PERMISSION [nama_database] INTO [nama_user];

-Program:

`char* grantPermission(char str[])
{
    char* ptr;
    char msg[BUFF];
    memset(msg, 0, BUFF);

    char dbName[BUFF];
    memset(dbName, 0, BUFF);

    char userid[BUFF];
    memset(userid, 0, BUFF);

    int i;
    char parse[BUFF];
    strcpy(parse, str);
    char* parseptr = parse;
    char* token;

    for (i = 0; token = strtok_r(parseptr, " ", &parseptr); i++)
    {
        if (i == 2)
        {
            strcpy(dbName, token);
        }
        else if (i == 4)
        {
            strncpy(userid, token, strlen(token) - 1);
        }
        else if (i == 3 && strcmp(token, "INTO"))
        {
            strcpy(msg, "SYNTAX ERROR!");
            ptr = msg;
            return ptr;
        }
    }

    FILE* file = fopen(fileUser, "r");
    char line[BUFF];
    int ada = 0;

    while (fgets(line, BUFF, file))
    {
        if (!strncmp(line, userid, strlen(userid)))
        {
            ada = 1;
            break;
        }
    }
    fclose(file);

    if (!ada)
    {
        strcpy(msg, "USER NOT FOUND!");
        ptr = msg;
        return ptr;
    }

    char namafile[BUFF];
    sprintf(namafile, "%s%s/grantedUser.txt", folderDB, dbName);
    
    file = fopen(namafile, "a+");
    if (!file)
    {
        strcpy(msg, "DATABASE NOT FOUND");
        ptr = msg;
        return ptr;
    }
    fprintf(file, "%s\n", userid);
    fclose(file);

    strcpy(msg, "ACCESS GRANTED");
    ptr = msg;
    logging(str, login.id);
    return ptr;
}

`

- Output:

![grant](dokumentasi/grant.png)


**C. DDL (Data Definition Language)**

- Bagian program yang digunakan untuk mendefinisikan dan mengelola struktur objek database dalam sistem manajemen database. Bertanggungjawab membuat dan menghapus objek database, seperti create database, create table, drop database, drop table dan drop column.


---**Membuat database**---

    Syntax: 

> CREATE DATABASE [nama database];


- Alur/Cara Kerja Program:

Program ini merupakan sebuah fungsi untuk membuat database.Fungsi ini akan menerima string input yang berisi perintah untuk membuat database. Nama database diambil dari token ketiga setelah pemisahan string. Direktori dengan nama database tersebut dibuat menggunakan fungsi mkdir. File permission untuk database juga dibuat menggunakan fungsi createFile. Hasil eksekusi program berupa pesan sukses "DATABASE SUCCESSFULLY CREATED!" jika database berhasil dibuat, atau pesan kesalahan "CANNOT CREATE DATABASE!" jika gagal membuat database. Program juga melakukan logging dengan memanggil fungsi logging untuk mencatat aktivitas atau perintah yang dilakukan pada database


- Program:

`char* createDB (char str[]){
	char* ptr;
	char msg[BUFF];

	char dbName[BUFF];
	//bzero(dbName, BUFF);
    memset(dbName, 0, BUFF);

	int i;
	char parse[BUFF];
	strcpy(parse, str);
	char* parseptr = parse;
	char* token;

	for (i = 0; token = strtok_r(parseptr, " ", &parseptr); i++){
		if (i == 2)
		{
			strncpy(dbName, token, strlen(token) - 1);
		}
	}

	char dbPath[BUFF];
	sprintf(dbPath, "%s%s", folderDB, dbName);
	char* pathdbptr = dbPath;

	if (mkdir(pathdbptr, 0777) != 0) {
		strcpy(msg, "CANNOT CREATE DATABASE!");
		ptr = msg;
		return ptr;
	}

	char permissionFile[BUFF];
	strcpy(permissionFile, dbPath);
	strcat(permissionFile, "/grantedUser.txt");

	createFile(permissionFile, login.id, "a+");

	strcpy(msg, "DATABASE SUCCESSFULLY CREATED!");
	ptr = msg;
    logging(str, login.id);
	return ptr;
}
`
- Output:

![createdb](dokumentasi/create db.jpg)


**---Menghapus Database---**

    Syntax:
	
> DROP DATABASE [nama database];

- Alur/Cara Kerja Program:

Program ini merupakan sebuah fungsi untuk menghapus (drop) database.Fungsi ini menerima string input yang berisi perintah untuk menghapus database.Nama database diambil dari token ketiga setelah pemisahan string.Program mencoba membuka direktori dengan nama database yang akan dihapus.Jika direktori tidak dapat dibuka, program mengembalikan pesan kesalahan "DATABASE NOT EXIST!".Jika direktori berhasil dibuka, program melanjutkan dengan membaca file permission.Jika file permission tidak dapat dibuka, program mengembalikan pesan kesalahan "USER HAS NO PERMISSION!". Jika file permission berhasil dibuka, program membaca setiap baris dalam file untuk memeriksa apakah pengguna memiliki izin akses. Jika pengguna memiliki izin akses, program menghapus seluruh direktori dan file yang terkait dengan database. Jika pengguna tidak memiliki izin akses, program mengembalikan pesan kesalahan "USER HAS NO PERMISSION!". Hasil eksekusi program berupa pesan sukses "DATABASE DROPPED." jika database berhasil dihapus, atau pesan kesalahan "FAILED TO DROP DATABASE." jika gagal menghapus database. Program juga melakukan logging dengan memanggil fungsi logging untuk mencatat aktivitas atau perintah yang dilakukan pada database.


- Program:


`char* dropDB(char str[])
{
	char* ptr;
	char msg[BUFF];

	char dbName[BUFF];

    memset(dbName, 0, BUFF);

	int i;
	char parse[BUFF];
	strcpy(parse, str);
	char* parseptr = parse;
	char* token;

	for (i = 0; token = strtok_r(parseptr, " ", &parseptr); i++)
	{
		if (i == 2)
		{
			strncpy(dbName, token, strlen(token) - 1);
		}
	}

	char dbPath[BUFF];
	sprintf(dbPath, "%s%s", folderDB, dbName);
	char* pathdbptr = dbPath;

    DIR* dir = opendir(dbPath);

	if (!dir) {
		strcpy(msg, "DATABASE NOT EXIST!");
		ptr = msg;
		return ptr;
	}
    closedir(dir);

	char permissionFile[BUFF];
    sprintf(permissionFile, "%s/%s", dbPath, "grantedUser.txt");


	FILE* file = fopen(permissionFile, "r");

    if (!file) {
        strcpy(msg, "USER HAS NO PERMISSION!");
        ptr = msg;
        return ptr;
    }

    char line[BUFF];
    memset(line, 0, BUFF);
    int hasPermission = 0;

	while (fgets(line, BUFF, file))
	{
        if (strstr(line, login.id) != NULL)
		{
            hasPermission = 1;
			break;
		}
	}

    fclose(file);

    if (hasPermission) {
        if (removeDir(dbPath) == 0) {
            strcpy(msg, "DATABASE DROPPED.");
            ptr = msg;
            logging(str, login.id);
            return ptr;
        }
        else {
            strcpy(msg, "FAILED TO DROP DATABASE.");
            ptr = msg;
            return ptr;
        }
    }
	strcpy(msg, "USER HAS NO PERMISSION!");
	ptr = msg;
    logging(str, login.id);
	return ptr;
}
`
- Output:

![dropdb](dokumentasi/drop db.png)


**---Membuat Tabel---**

    Syntax:
> CREATE TABLE [nama tabel];


- Alur/Cara Kerja Program:

Fungsi createTable digunakan untuk membuat tabel (table) dalam sebuah database. Fungsi ini menerima string input yang berisi perintah untuk membuat tabel. Nama tabel diambil dari token kedua setelah pemisahan string. Fungsi ini memeriksa keberadaan karakter "(" dalam string input untuk memastikan validitas perintah. Jika karakter tersebut tidak ditemukan, fungsi mengembalikan pesan kesalahan "INVALID COMMAND.". Jika perintah valid, fungsi membuat file tabel dengan menggunakan path yang terdiri dari direktori database, database saat ini, dan nama tabel dengan ekstensi file .csv. Jika file tabel berhasil dibuat, fungsi melanjutkan dengan memproses token-token kolom dalam tabel. Setiap kolom dipisahkan menggunakan tanda koma (",") dalam string input. Kolom tersebut kemudian diproses untuk mendapatkan nama kolom dan tipe data kolom. Nama kolom dan tipe data kolom dipisahkan menggunakan tanda spasi (" ") dalam string input. Setiap kolom dan tipe data kolom ditulis ke dalam file tabel yang telah dibuat. Setiap kolom dan tipe data kolom dipisahkan menggunakan tanda titik dua (":") dan diakhiri dengan tanda tab ("\t") dalam file tabel. Setelah selesai menulis kolom-kolom ke dalam file tabel, file tabel ditutup. Hasil eksekusi fungsi ini berupa pesan sukses "TABLE CREATED." jika tabel berhasil dibuat, atau pesan kesalahan "FAILED TO CREATE TABLE." jika terjadi kesalahan dalam pembuatan tabel. Fungsi ini juga melakukan logging aktivitas atau perintah yang dilakukan pada database

- Program:

`char* createTable(char str[]) {

	char* ptr;
	char msg[BUFF];

	char tableName[BUFF];
    memset(tableName, 0, BUFF);
	
	int i = 0;
	char cmd[BUFF];
	strcpy(cmd, str);
	char* cmdptr = cmd;
	char* token;

	for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++)
	{
		if (i == 2)
		{
			strcpy(tableName, token);
		}
	}

    char* bracketPtr = strchr(str, '(');
    if (!bracketPtr)
    {
        strcpy(msg, "INVALID COMMAND.");
        ptr = msg;
        return ptr;
    }

    char* cmdptr1 = bracketPtr + 1;
    strncpy(cmd, cmdptr1, strlen(cmdptr1) - 2);
    char temp[BUFF];
    strcpy(temp, cmd);
    
	char tablePath[BUFF];
	sprintf(tablePath, "%s%s/%s.csv", folderDB, currentDB, tableName);

	FILE* file = fopen(tablePath, "w+");
    if (!file) {
        strcpy(msg, "FAILED TO CREATE TABLE.");
        ptr = msg;
        return ptr;
    }   


	char* token2;
	int j = 0;
	
	char columnName[BUFF], columnData[BUFF];
	for (i = 0; token = strtok_r(temp, ",", &temp); i++)
	{
		char column[BUFF];
		strcpy(column, token);
        char* columnPtr = column;
		for (j = 0; token2 = strtok_r(columnPtr, " ", &columnPtr); j++)
		{
			if (j == 0)
			{
				strcpy(columnName, token2);
			}
			if (j == 1)
			{
				strcpy(columnData, token2);
			}
		}
        fprintf(file, "%s:%s", columnName, columnData);
        fprintf(file, "\t");
	}
	fclose(file);

	strcpy(msg, "TABLE CREATED.");
	ptr = msg;
    logging(str, login.id);
	return ptr;
}
`
- Output:

![createtable](dokumentasi/create table.jpg)


**---Menghapus Tabel---**

    Syntax:
> DROP TABLE [nama tabel];

- Alur/Cara Kerja Program:

Fungsi dropTable digunakan untuk menghapus sebuah tabel (table) dalam sebuah database. Fungsi ini menerima string input yang berisi perintah untuk menghapus tabel. Nama tabel diambil dari token ketiga setelah pemisahan string. Fungsi ini membentuk path lengkap ke file tabel yang akan dihapus berdasarkan direktori database, database saat ini, dan nama tabel dengan ekstensi file .csv. Fungsi mencoba membuka file tabel dalam mode "r" (read). Jika file tidak dapat dibuka, fungsi mengembalikan pesan kesalahan "TABLE NOT FOUND.". Jika file tabel berhasil dibuka, artinya tabel ditemukan, dan file tabel ditutup. Fungsi ini melakukan proses fork untuk membuat child process yang akan menjalankan perintah sistem rm untuk menghapus file tabel. Jika fork gagal, fungsi mengembalikan pesan kesalahan "FAILED TO DROP TABLE.". Jika fork berhasil dan program berjalan pada child process, fungsi menggunakan execv untuk menjalankan perintah rm dengan argumen path file tabel yang akan dihapus. Jika child process selesai dieksekusi, program keluar dari child process dan kembali ke parent process.
Pada parent process, fungsi menunggu (wait) hingga child process selesai dieksekusi.
Setelah child process selesai dieksekusi, parent process memeriksa status keluaran dari child process. Jika status keluaran (WEXITSTATUS) sama dengan 0, artinya perintah rm berhasil dijalankan dan file tabel berhasil dihapus. Fungsi mengembalikan pesan sukses "TABLE DROPPED.". Jika status keluaran tidak sama dengan 0, artinya terjadi kesalahan dalam menjalankan perintah rm dan file tabel tidak berhasil dihapus.Fungsi mengembalikan pesan kesalahan "FAILED TO DROP TABLE.". Fungsi ini juga melakukan logging aktivitas atau perintah yang dilakukan pada database.


- Program:

`char* dropTable(char str[]){
    char* ptr;
	char msg[BUFF];
 
	char tableName[BUFF];
    memset(tableName, 0, BUFF);
 
	// temp
	int i = 0;
	char cmd[BUFF];
	strcpy(cmd, str);
	char* cmdptr = cmd;
	char* token;
 
	// DROP TABLE 
	for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++)
	{
		if (i == 2)
		{
			strncpy(tableName, token, strlen(token) - 1);
		}
	}
 
	char tablePath[BUFF];
	sprintf(tablePath, "%s%s/%s.csv", folderDB, currentDB, tableName);
 
	FILE* file = fopen(tablePath, "r");
    if (!file)
    {
        strcpy(msg, "TABLE NOT FOUND.");
        ptr = msg;
        return ptr;
    }
    fclose(file);
 
	int status;
	pid_t child_id = fork();
  
    if (child_id < 0) {
        strcpy(msg, "FAILED TO DROP TABLE.");
        ptr = msg;
        return ptr;
    } else if (child_id == 0)
    {
        // Child process
        char* argv[] = {"rm", tablePath, NULL};
        execv("/bin/rm", argv);
        exit(EXIT_FAILURE);
    } else {
        // Parent process
        wait(&status);
        if (WEXITSTATUS(status) == 0)
        {
            strcpy(msg, "TABLE DROPPED.");
            ptr = msg;
            logging(str, login.id);
            return ptr;
        }
        else
        {
            strcpy(msg, "FAILED TO DROP TABLE.");
            ptr = msg;
            return ptr;
        }
    }   
	
}`

- Output:

![droptable](dokumentasi/drop table.png)


**---Menghapus Kolom---**

    Syntax:
> DROP COLUMN [nama kolom] FROM [nama tabel];

- Alur/Cara Kerja Program:

Fungsi menerima string str yang berisi perintah untuk menghapus baris dari sebuah tabel.
Perintah diproses dengan membaginya menjadi token-token menggunakan fungsi strtok_r.
Nama tabel dan kondisi kolom dan nilai kondisi diekstraksi dari perintah.
Dilakukan pemeriksaan kesalahan, seperti memeriksa apakah database sudah dipilih, apakah perintah valid, dan apakah tabel ditemukan. File tabel dibuka untuk dibaca, dan jika gagal, program mencetak pesan kesalahan dan mengembalikan NULL. File sementara dibuat untuk menyimpan baris-baris yang tidak dihapus. Program membaca setiap baris dalam file tabel, memeriksa nilai kondisi pada kolom yang ditentukan, dan menulis baris tersebut ke file sementara jika kondisi tidak cocok.
Jika tidak ada baris yang dihapus, program mencetak pesan "NO MATCHING RECORDS FOUND" dan menghapus file sementara. Jika ada baris yang dihapus, file tabel asli dihapus, dan file sementara diubah namanya menjadi file tabel asli. Program mencetak pesan "DATA DELETED" untuk menandakan bahwa proses penghapusan selesai.

-  Program:

`char* delete_from_table(char str[]) {
    if (strlen(currentDB) == 0) {
        printf("NO DATABASE SELECTED.\n");
        return;
    }

    char tableName[BUFF];
    char conditionColumn[BUFF];
    char conditionValue[BUFF];
    memset(tableName, 0, BUFF);
    memset(conditionColumn, 0, BUFF);
    memset(conditionValue, 0, BUFF);

    int i = 0;
    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++) {
        if (i == 2) {
            strcpy(tableName, token);
        } else if (i == 4) {
            if (strcmp(token, "WHERE") != 0) {
                printf("INVALID COMMAND. Missing 'WHERE'.\n");
                return;
            }
        } else if (i % 4 == 1) {
            strcpy(conditionColumn, token);
        } else if (i % 4 == 3) {
            strcpy(conditionValue, token);
        }
    }

    if (strlen(tableName) == 0 || strlen(conditionColumn) == 0 || strlen(conditionValue) == 0) {
        printf("INVALID COMMAND.\n");
        return;
    }

    char filename[BUFF];
    sprintf(filename, "%s%s/%s.txt", folderDB, currentDB, tableName);

    FILE* file = fopen(filename, "r");
    if (!file) {
        printf("TABLE NOT FOUND.\n");
        return;
    }

    char tempFilename[BUFF];
    sprintf(tempFilename, "%s%s/%s_temp.txt", folderDB, currentDB, tableName);

    FILE* tempFile = fopen(tempFilename, "w");
    if (!tempFile) {
        printf("FAILED TO OPEN TEMPORARY FILE.\n");
        fclose(file);
        return;
    }

    char line[BUFF];
    int deleted = 0;
    while (fgets(line, BUFF, file)) {
        char tempLine[BUFF];
        strcpy(tempLine, line);
        char* token = strtok(tempLine, ",");
        int columnFound = 0;
        while (token != NULL) {
            char tempColumnValue[BUFF];
            strcpy(tempColumnValue, token);
            if (strcmp(token, conditionColumn) == 0) {
                if (strcmp(conditionValue, tempColumnValue) == 0) {
                    columnFound = 1;
                    break;
                }
            }
            token = strtok(NULL, ",");
        }
        if (!columnFound) {
            fprintf(tempFile, "%s", line);
        } else {
            deleted = 1;
        }
    }

    fclose(file);
    fclose(tempFile);

    if (!deleted) {
        printf("NO MATCHING RECORDS FOUND.\n");
        remove(tempFilename);
        return;
    }

    remove(filename);
    rename(tempFilename, filename);

    printf("DATA DELETED.\n");
}

char* drop_column(char str[]) {
    if (strlen(currentDB) == 0) {
        printf("NO DATABASE SELECTED.\n");
        return;
    }

    char columnName[BUFF];
    char tableName[BUFF];
    memset(columnName, 0, BUFF);
    memset(tableName, 0, BUFF);

    int i = 0;
    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++) {
        if (i == 2) {
            strcpy(columnName, token);
        } else if (i == 4) {
            if (strcmp(token, "FROM") != 0) {
                printf("INVALID COMMAND. Missing 'FROM'.\n");
                return;
            }
        } else if (i == 5) {
            strcpy(tableName, token);
            break;
        }
    }

    if (strlen(columnName) == 0 || strlen(tableName) == 0) {
        printf("INVALID COMMAND.\n");
        return;
    }

    char filename[BUFF];
    sprintf(filename, "%s%s/%s.txt", folderDB, currentDB, tableName);

    FILE* file = fopen(filename, "r");
    if (!file) {
        printf("TABLE NOT FOUND.\n");
        return;
    }

    char tempFilename[BUFF];
    sprintf(tempFilename, "%s%s/%s_temp.txt", folderDB, currentDB, tableName);

    FILE* tempFile = fopen(tempFilename, "w");
    if (!tempFile) {
        printf("FAILED TO OPEN TEMPORARY FILE.\n");
        fclose(file);
        return;
    }

    char line[BUFF];
    int columnDeleted = 0;
    while (fgets(line, BUFF, file)) {
        char tempLine[BUFF];
        strcpy(tempLine, line);

        char* token = strtok(tempLine, ",");
        int columnCount = 0;
        while (token != NULL) {
            char tempColumnName[BUFF];
            strcpy(tempColumnName, token);
            if (strcmp(columnName, tempColumnName) == 0) {
                columnDeleted = 1;
            } else {
                if (columnCount > 0) {
                    fprintf(tempFile, ",");
                }
                fprintf(tempFile, "%s", token);
                columnCount++;
            }
            token = strtok(NULL, ",");
        }

        if (columnCount > 0) {
            fprintf(tempFile, "\n");
        }
    }

    fclose(file);
    fclose(tempFile);

    if (!columnDeleted) {
        printf("COLUMN NOT FOUND.\n");
        remove(tempFilename);
        return;
    }

    remove(filename);
    rename(tempFilename, filename);

    printf("COLUMN DROPPED.\n");
}
`

- Output:

![datadelete](dokumentasi/delete data.png)

![dropcolumn](dokumentasi/drop column.png)

**D. DML (Data Manipulation Language)**

**---INSERT---**

	Syntax:
	> INSERT 

- Alur/Cara Kerja Program:

Fungsi menerima perintah dalam bentuk string str untuk memilih data dari tabel.
Dilakukan pemeriksaan kesalahan, seperti memeriksa apakah database sudah dipilih dan apakah perintah valid. Nama tabel diekstraksi dari perintah. File tabel dibuka untuk dibaca, dan jika gagal, program mencetak pesan kesalahan dan mengembalikan NULL. Setiap baris dalam file tabel dibaca, dan jika ada kolom dan nilai yang ditentukan, dilakukan filtering untuk mencetak baris yang sesuai. Jika kolom dan nilai tidak ditentukan, semua baris dicetak.
File ditutup setelah selesai membaca.

- Program:

`char* insert_table(char str[]) {
    if (strlen(currentDB) == 0) {
        printf("NO DATABASE SELECTED.\n");
        return;
    }

    char tableName[BUFF];
    char values[BUFF];
    memset(tableName, 0, BUFF);
    memset(values, 0, BUFF);

    int i = 0;
    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++) {
        if (i == 1 && strcmp(token, "INTO") != 0) {
            printf("INVALID COMMAND. Missing 'INTO'.\n");
            return;
        }
        if (i == 2) {
            strcpy(tableName, token);
        }
        if (i == 3 && strcmp(token, "(") != 0) {
            printf("INVALID COMMAND. Missing '('.\n");
            return;
        }
        if (i > 3 && strcmp(token, ")") == 0) {
            break;
        }
        if (i > 3) {
            strcat(values, token);
            strcat(values, " ");
        }
    }

    if (strlen(tableName) == 0 || strlen(values) == 0) {
        printf("INVALID COMMAND.\n");
        return;
    }

    // Menghilangkan spasi ekstra di akhir daftar nilai
    values[strlen(values) - 1] = '\0';

    char filename[BUFF];
    sprintf(filename, "%s%s/%s.txt", folderDB, currentDB, tableName);

    FILE* file = fopen(filename, "a");
    if (!file) {
        printf("FAILED TO OPEN TABLE.\n");
        return;
    }

    fprintf(file, "%s\n", values);
    fclose(file);

    printf("DATA INSERTED.\n");
}
`

- Output:

![insert](dokumentasi/logging.png)


**---UPDATE---**

	Syntax:
> 

- Alur/Cara Kerja Program:

Fungsi update_table menerima perintah SQL dalam bentuk string dan memprosesnya untuk memperbarui data dalam tabel yang terkait.

Fungsi melakukan pengecekan awal untuk memastikan bahwa database yang sedang dipilih telah ditentukan.
Token-token perintah SQL diproses dan nilai-nilai yang relevan disimpan dalam variabel yang sesuai.
Setelah itu, fungsi membuka file tabel yang terkait untuk memulai proses pembaruan.
Fungsi membaca baris-baris dalam file tabel, memeriksa kolom yang sesuai dengan kolom yang ingin diperbarui, dan menggantinya dengan nilai baru.Hasil pembaruan ditulis ke dalam file temporary.
Setelah semua baris diproses, file asli dihapus dan file temporary diubah namanya menjadi file asli.
Jika ada kolom yang ditemukan dan diperbarui, fungsi mencetak pesan "TABLE UPDATED" sebagai konfirmasi pembaruan yang berhasil.Jika tidak ada kolom yang ditemukan dan diperbarui, fungsi mencetak pesan "NO MATCHING RECORDS FOUND" dan tidak ada perubahan yang terjadi pada tabel.
Jika ada kesalahan dalam perintah SQL, fungsi mencetak pesan yang sesuai seperti "INVALID COMMAND" atau "INVALID COMMAND. Missing 'SET'".Fungsi juga dapat mencetak pesan "TABLE NOT FOUND" jika file tabel yang dimaksud tidak ditemukan.Fungsi memastikan untuk menutup file yang dibuka dan menghapus file temporary jika diperlukan. 

- Program:

`char* update_table(char str[]) {
    if (strlen(currentDB) == 0) {
        printf("NO DATABASE SELECTED.\n");
        return;
    }

    char tableName[BUFF];
    char columnName[BUFF];
    char columnValue[BUFF];
    char conditionColumn[BUFF];
    char conditionValue[BUFF];
    memset(tableName, 0, BUFF);
    memset(columnName, 0, BUFF);
    memset(columnValue, 0, BUFF);
    memset(conditionColumn, 0, BUFF);
    memset(conditionValue, 0, BUFF);

    int i = 0;
    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++) {
        if (i == 1) {
            strcpy(tableName, token);
        } else if (i == 3) {
            if (strcmp(token, "SET") != 0) {
                printf("INVALID COMMAND. Missing 'SET'.\n");
                return;
            }
        } else if (i % 4 == 0) {
            if (strcmp(token, "WHERE") == 0) {
                break;
            } else {
                strcpy(columnName, token);
            }
        } else if (i % 4 == 2) {
            strcpy(columnValue, token);
        } else if (i % 4 == 3) {
            if (strcmp(token, "WHERE") != 0) {
                printf("INVALID COMMAND. Missing 'WHERE'.\n");
                return;
            }
        } else if (i % 4 == 1) {
            strcpy(conditionColumn, token);
        } else if (i % 4 == 3) {
            strcpy(conditionValue, token);
        }
    }

    if (strlen(tableName) == 0 || strlen(columnName) == 0 || strlen(columnValue) == 0 ||
        strlen(conditionColumn) == 0 || strlen(conditionValue) == 0) {
        printf("INVALID COMMAND.\n");
        return;
    }

    char filename[BUFF];
    sprintf(filename, "%s%s/%s.txt", folderDB, currentDB, tableName);

    FILE* file = fopen(filename, "r");
    if (!file) {
        printf("TABLE NOT FOUND.\n");
        return;
    }

    char tempFilename[BUFF];
    sprintf(tempFilename, "%s%s/%s_temp.txt", folderDB, currentDB, tableName);

    FILE* tempFile = fopen(tempFilename, "w");
    if (!tempFile) {
        printf("FAILED TO OPEN TEMPORARY FILE.\n");
        fclose(file);
        return;
    }

    char line[BUFF];
    int updated = 0;
    while (fgets(line, BUFF, file)) {
        char tempLine[BUFF];
        strcpy(tempLine, line);
        char* token = strtok(tempLine, ",");
        int columnFound = 0;
        while (token != NULL) {
            char tempColumnValue[BUFF];
            strcpy(tempColumnValue, token);
            if (strcmp(token, columnName) == 0) {
                columnFound = 1;
                strcpy(tempColumnValue, columnValue);
            }
            fprintf(tempFile, "%s,", tempColumnValue);
            token = strtok(NULL, ",");
        }
        if (columnFound) {
            updated = 1;
        }
        fprintf(tempFile, "\n");
    }

    fclose(file);
    fclose(tempFile);

    if (!updated) {
        printf("NO MATCHING RECORDS FOUND.\n");
        remove(tempFilename);
        return;
    }

    remove(filename);
    rename(tempFilename, filename);

    printf("DATA UPDATED.\n");
}

`

- Output:

![update](dokumentasi/update.png)


**---DELETE---**

	Syntax:

> 

- Alur/Cara Kerja Program:

Fungsi ini digunakan untuk menghapus baris-baris yang memenuhi kondisi tertentu dari sebuah tabel.
Pertama, fungsi memeriksa apakah database sudah dipilih. Jika tidak, maka fungsi mencetak pesan "NO DATABASE SELECTED" dan mengembalikan NULL.
Selanjutnya, fungsi melakukan ekstraksi nama tabel, kolom kondisi, dan nilai kondisi dari string perintah. Dilakukan pemeriksaan kesalahan, seperti memeriksa apakah semua argumen yang diperlukan telah diberikan. Jika tidak, maka fungsi mencetak pesan "INVALID COMMAND" dan mengembalikan NULL.
Fungsi membuka file tabel yang sesuai untuk dibaca. Jika file tidak dapat dibuka, fungsi mencetak pesan "TABLE NOT FOUND" dan mengembalikan NULL. Fungsi membuat file sementara untuk menyimpan data sementara selama proses penghapusan. Setiap baris dalam file tabel dibaca. Nilai pada kolom kondisi dibandingkan dengan nilai kondisi yang diberikan. Jika tidak cocok, baris tersebut ditulis ke dalam file sementara. Jika cocok, baris tersebut dianggap terhapus.
Setelah selesai membaca, file tabel dan file sementara ditutup.
Jika tidak ada baris yang terhapus, fungsi mencetak pesan "NO MATCHING RECORDS FOUND" dan menghapus file sementara. Jika ada baris yang terhapus, file asli dihapus, dan file sementara diubah namanya menjadi nama file asli. Terakhir, fungsi mencetak pesan "RECORDS DELETED" untuk menandakan keberhasilan penghapusan.

- Program:

`char* delete_from_table(char str[]) {
    if (strlen(currentDB) == 0) {
        printf("NO DATABASE SELECTED.\n");
        return;
    }

    char tableName[BUFF];
    char conditionColumn[BUFF];
    char conditionValue[BUFF];
    memset(tableName, 0, BUFF);
    memset(conditionColumn, 0, BUFF);
    memset(conditionValue, 0, BUFF);

    int i = 0;
    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++) {
        if (i == 2) {
            strcpy(tableName, token);
        } else if (i == 4) {
            if (strcmp(token, "WHERE") != 0) {
                printf("INVALID COMMAND. Missing 'WHERE'.\n");
                return;
            }
        } else if (i % 4 == 1) {
            strcpy(conditionColumn, token);
        } else if (i % 4 == 3) {
            strcpy(conditionValue, token);
        }
    }

    if (strlen(tableName) == 0 || strlen(conditionColumn) == 0 || strlen(conditionValue) == 0) {
        printf("INVALID COMMAND.\n");
        return;
    }

    char filename[BUFF];
    sprintf(filename, "%s%s/%s.txt", folderDB, currentDB, tableName);

    FILE* file = fopen(filename, "r");
    if (!file) {
        printf("TABLE NOT FOUND.\n");
        return;
    }

    char tempFilename[BUFF];
    sprintf(tempFilename, "%s%s/%s_temp.txt", folderDB, currentDB, tableName);

    FILE* tempFile = fopen(tempFilename, "w");
    if (!tempFile) {
        printf("FAILED TO OPEN TEMPORARY FILE.\n");
        fclose(file);
        return;
    }

    char line[BUFF];
    int deleted = 0;
    while (fgets(line, BUFF, file)) {
        char tempLine[BUFF];
        strcpy(tempLine, line);
        char* token = strtok(tempLine, ",");
        int columnFound = 0;
        while (token != NULL) {
            char tempColumnValue[BUFF];
            strcpy(tempColumnValue, token);
            if (strcmp(token, conditionColumn) == 0) {
                if (strcmp(conditionValue, tempColumnValue) == 0) {
                    columnFound = 1;
                    break;
                }
            }
            token = strtok(NULL, ",");
        }
        if (!columnFound) {
            fprintf(tempFile, "%s", line);
        } else {
            deleted = 1;
        }
    }

    fclose(file);
    fclose(tempFile);

    if (!deleted) {
        printf("NO MATCHING RECORDS FOUND.\n");
        remove(tempFilename);
        return;
    }

    remove(filename);
    rename(tempFilename, filename);

    printf("DATA DELETED.\n");
}
`

- Output:

![delete](dokumentasi/delete data.png)

**---SELECT---**

	Syntax:

> 

- Alur/Cara Kerja Program:

- Program:

`char* select_table(char str[]) {
    if (strlen(currentDB) == 0) {
        printf("NO DATABASE SELECTED.\n");
        return;
    }

    char tableName[BUFF];
    char columnName[BUFF];
    char value[BUFF];
    memset(tableName, 0, BUFF);
    memset(columnName, 0, BUFF);
    memset(value, 0, BUFF);

    int i = 0;
    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;

    // Menggunakan strtok_r untuk mendapatkan token yang diinginkan
    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++) {
        if (i == 3 && strcmp(token, "*") == 0) {
            token = strtok_r(NULL, " ", &cmdptr);  // Memeriksa kata "FROM"
            if (token && strcmp(token, "FROM") == 0) {
                token = strtok_r(NULL, " ", &cmdptr);  // Mendapatkan nama tabel
                if (token) {
                    strcpy(tableName, token);
                    break;
                }
            }
        }
    }

    if (strlen(tableName) == 0) {
        printf("INVALID COMMAND.\n");
        return;
    }

    token = strtok_r(NULL, " ", &cmdptr);  // Memeriksa kata "WHERE"
    if (token && strcmp(token, "WHERE") == 0) {
        token = strtok_r(NULL, "=", &cmdptr);  // Mendapatkan nama kolom
        if (token) {
            strcpy(columnName, token);
            token = strtok_r(NULL, " ", &cmdptr);  // Memeriksa operator "="
            if (token && strcmp(token, "=") == 0) {
                token = strtok_r(NULL, " ", &cmdptr);  // Mendapatkan value
                if (token) {
                    strcpy(value, token);
                }
            }
        }
    }

    char filename[BUFF];
    sprintf(filename, "%s%s/%s.txt", folderDB, currentDB, tableName);

    FILE* file = fopen(filename, "r");
    if (!file) {
        printf("TABLE NOT FOUND.\n");
        return;
    }

    char line[BUFF];
    while (fgets(line, BUFF, file)) {
        // Jika kolom dan value tidak kosong, lakukan filtering berdasarkan nilai kolom
        if (strlen(columnName) > 0 && strlen(value) > 0) {
            char* columnValue = strtok(line, ",");
            if (columnValue && strcmp(columnValue, columnName) == 0) {
                columnValue = strtok(NULL, ",");
                if (columnValue && strcmp(columnValue, value) == 0) {
                    printf("%s", line);
                    printf("DATA SELECTED.\n");
                }
            }
        }
        // Jika kolom dan value kosong, cetak semua baris
        else {
            printf("%s", line);
            printf("DATA SELECTED.\n");
        }
    }

    fclose(file);
}
`

- Output:

![select](dokumentasi/select all.png)

**---WHERE---**

	Syntax:

> 

- Alur/Cara Kerja Program:

- Program:

- Output:

![where](dokumentasi/where.png)

5. Logging

**---Menampilkan log pada setiap command---**

	Format:
	> timestamp(yyyy-mm-dd hh:mm:ss):username:command

- Output:

![logging](dokumentasi/logging.png)


## --- client.sh

### Penyelesaian

```
#define PORT 8080
#define SIZE 100
#define BUFF 1024
```
Merupakan direktif pra-pemrosesan yang mendefinisikan konstanta untuk nomor port (PORT), ukuran array (SIZE), dan ukuran buffer (BUFF).

```
int sock = 0;
int valread; 
int uid;
int auth_ = 0;
char userlog[SIZE]={};
char username[BUFF], msg_uid[BUFF];
```

Ini adalah variabel global yang digunakan dalam program. 
`sock` adalah deskriptor soket, `valread` menyimpan jumlah byte yang dibaca dari soket, 
`uid` menyimpan ID pengguna, `auth_` mewakili status autentikasi, dan `userlog`, `username`, 
dan `msg_uid` adalah array karakter untuk menyimpan informasi terkait pengguna.

```
    struct sockaddr_in address;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    
    char buffer[BUFF] = {0};
```

Merupakan struktur dan variabel yang digunakan untuk komunikasi soket. address dan serv_addr adalah struktur sockaddr_in yang mewakili alamat klien dan server. 
hello adalah pointer ke pesan sambutan yang akan dikirimkan ke server. buffer adalah array yang menyimpan data yang diterima dari server.

```
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
```

Di sini, soket dibuat menggunakan fungsi socket.
Fungsi ini mengambil tiga argumen: domain alamat (AF_INET untuk IPv4), tipe soket (SOCK_STREAM untuk TCP), dan protokol (0 untuk IP).

```
    memset(&serv_addr, '0', sizeof(serv_addr));
```

Fungsi memset digunakan untuk mengisi struktur serv_addr dengan nol.

```
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
```

Di sini, detail alamat server ditetapkan. Alamat IP diatur menjadi "127.0.0.1" (localhost), keluarga alamat diatur ke AF_INET, 
dan nomor port diatur ke PORT (8080).

```
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
```

Fungsi inet_pton digunakan untuk mengonversi alamat server dari teks ke bentuk biner. Jika konversi gagal, pesan kesalahan dicetak, dan program keluar.

```
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nFailed to Connect \n");
        return -1;
    }
```

Fungsi connect digunakan untuk membuat koneksi dengan server. Fungsi ini mengambil deskriptor soket (sock), alamat server (serv_addr), 
dan ukuran struktur alamat. Jika koneksi gagal, pesan kesalahan dicetak, dan program keluar.

```
    uid=getuid();
    sprintf(msg_uid, "%d", uid);
    send(sock, msg_uid, strlen(msg_uid), 0); //Pesan UID dikirim ke server
```

Fungsi getuid mengambil ID pengguna (UID) dari pengguna saat ini dan menyimpannya dalam variabel uid.
Fungsi sprintf digunakan untuk mengonversi uid menjadi format string dan menyimpannya dalam array msg_uid. Kemudian, fungsi send mengirim pesan UID ke server.

```
    bool isRoot;
    if(uid) isRoot=false;
    else if(!uid) isRoot=true;
```

Variabel isRoot digunakan untuk menentukan apakah pengguna adalah pengguna root.
Jika UID non-nol, isRoot diatur menjadi false; jika tidak, diatur menjadi true.

```
    if(!isRoot)
    {
        read(sock, buffer, 1024); 
        bzero(buffer, 1024);
        sprintf(buffer, "%s:%s\n", argv[2], argv[4]);
        send(sock, buffer, strlen(buffer), 0);
    }
```

Jika pengguna bukan pengguna root, klien membaca pesan dari server menggunakan fungsi read.
Kemudian, buffer dikosongkan, dan string format yang berisi nama pengguna dan kata sandi (diperoleh dari argumen baris perintah argv[2] dan argv[4])
disimpan dalam buffer. Akhirnya, buffer dikirimkan ke server menggunakan fungsi send.

```
    if(isRoot) strcpy(username, "Root");
    else strcpy(username, argv[2]);
```

Jika pengguna adalah pengguna root (isRoot adalah true), username diatur sebagai "Root"; jika tidak, diatur sebagai nama pengguna yang diberikan pada argv[2].

```
    bzero(buffer, 1024);
    read(sock, buffer, 1024);
    
    printf("%s\n", buffer);
    if (strcmp(buffer, "Invalid Username or Password!") == 0) {
        return 0;
    }
```

buffer dikosongkan, dan pesan dari server dibaca ke dalam buffer menggunakan fungsi read.
Pesan yang diterima kemudian dicetak. Jika pesan yang diterima adalah "Invalid Username or Password!", program keluar.

```
    char query[255];

    while (1) {
        printf("[%s]> ", username);
        scanf(" %[^\n]", query);
        send(sock, query, strlen(query), 0);

        bzero(buffer, 1024);
        read(sock, buffer, 1024);

        printf("%s\n", buffer);
    }
```

Dalam lingkaran while yang tak terhingga, pengguna diminta dengan baris perintah dalam format [username]> .
Input pengguna dibaca menggunakan scanf dan disimpan dalam array query.
Permintaan pengguna kemudian dikirim ke server menggunakan fungsi send. buffer dikosongkan,
dan respon dari server diterima menggunakan fungsi read. Akhirnya, pesan yang diterima dicetak.
Loop ini berlanjut hingga program dihentikan.

## --- dockerfile

```
FROM ubuntu:latest
```

Menentukan gambar dasar (base image) untuk kontainer Docker. Kami menggunakan versi terbaru dari distribusi Ubuntu Linux sebagai gambar dasar.

```
RUN apt-get update && \
    apt-get install -y gcc sudo
```

Melakukan pembaruan daftar paket pada gambar Ubuntu dan menginstal compiler gcc serta paket sudo.

```
RUN useradd -ms /bin/bash myuser
```

Membuat pengguna baru dengan nama "myuser" yang memiliki direktori home /home/myuser dan shell bawaan /bin/bash.

```
WORKDIR /home/myuser
```

Mengatur direktori kerja di dalam kontainer menjadi /home/myuser.
 
```
COPY ../database/database.c ./database/
COPY ../client/client.c ./client/
```

Menyalin file kode sumber database.c dan client.c dari mesin lokal (dengan path relatif ../database/database.c dan ../client/client.c) 
ke direktori yang sesuai di dalam kontainer, yaitu ./database/ dan ./client/.

```
RUN gcc -pthread ./database/database.c -o ./database/database
RUN gcc ./client/client.c -o ./client/client
```

Mengompilasi file kode sumber database.c dan client.c di dalam kontainer menggunakan compiler gcc. Hasilnya adalah file biner (executable) yang diberi nama database dan client, dan ditempatkan di direktori masing-masing, yaitu ./database/ dan ./client/.

```
CMD ["bash", "-c", "./database/database & sleep 7 && ./client/client"]
```

Menentukan perintah yang akan dijalankan saat kontainer dimulai. Perintah ini menjalankan program database dalam latar belakang (&), menunggu selama 7 detik (sleep 7), dan kemudian menjalankan program client.

![dockerbuild](dokumentasi/docker build.png)

![dockerpush](dokumentasi/docker push.png)

![dockerrun](dokumentasi/docker run.png)

# Kendala
- Terdapat kesulitan dalam push ke hubdocker.
- Table yang dibuat memiliki memori yang sangat berdasarkan.
- Program dump belum sempat diselesaikan.
