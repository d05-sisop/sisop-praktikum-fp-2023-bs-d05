#include <stdio.h>
#include <sys/socket.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <ctype.h>
#include <fcntl.h>
#define PORT 8080
#define SIZE 100
#define BUFF 1024

//Variabel-vaariabel yang digunakan dalam program untuk menyimpan data
int sock = 0;
int valread; 
int uid;
int auth_ = 0;
char userlog[SIZE]={};
char username[BUFF], msg_uid[BUFF];

int main(int argc, char const *argv[]) {

    // base 
    //struktur untuk menyimpan informasi alamat server dan alamat klien
    struct sockaddr_in address;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    
    // Menggunakan buffer untuk menerima pesan dari server.
    char buffer[BUFF] = {0};
    
    //Membuat soket baru. Hasilnya disimpan dalam variabel sock
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
    
    //Fungsi memset untuk menginisialisasi struktur serv_addr dengan nol.
    memset(&serv_addr, '0', sizeof(serv_addr));
    
    //Alamat server ditetapkan dengan mengatur alamat IP (127.0.0.1) dan port (PORT) pada struktur serv_addr
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    
    /*Fungsi inet_pton() untuk mengkonversi alamat IP dari format string
    menjadi format biner dan menyimpannya dalam struktur serv_addr*/
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
    	
    	//Jika alamat tidak valid, pesan kesalahan akan dicetak dan program keluar dengan nilai -1.
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nFailed to Connect \n");
        return -1;
    }
	//UID pengguna (uid) diambil menggunakan fungsi getuid() dan disimpan dalam variabel uid.
    uid=getuid();
    
    //UID dikonversi menjadi string menggunakan fungsi sprintf() dan disimpan dalam msg_uid.
    sprintf(msg_uid, "%d", uid);
    send(sock, msg_uid, strlen(msg_uid), 0); //Pesan UID dikirim ke server 

    bool isRoot;
	//Jika UID bukan nol, maka pengguna bukan root (false), jika UID nol, maka pengguna adalah root (true).
    if(uid) isRoot=false;
    else if(!uid) isRoot=true;
	
	
	/*Jika pengguna bukan root, program akan membaca pesan dari server menggunakan fungsi read(), 
	mengosongkan buffer, dan mengisi buffer dengan format "<username>:<password>" 
	dari argumen baris perintah*/
    if(!isRoot)
    {
        read(sock, buffer, 1024); 
        bzero(buffer, 1024);
        sprintf(buffer, "%s:%s\n", argv[2], argv[4]);
        send(sock, buffer, strlen(buffer), 0);
    }

	/*Jika pengguna adalah root, username diatur sebagai "Root". 
	Jika bukan root, username diambil dari argumen baris perintah*/
    if(isRoot) strcpy(username, "Root");
    else strcpy(username, argv[2]);

    bzero(buffer, 1024);
    read(sock, buffer, 1024);
    
    printf("%s\n", buffer);
    //Jika pesan respons adalah "Wrong Username or Password!", program akan mengembalikan nilai 0 dan keluar
    if (strcmp(buffer, "Invalid Username or Password!") == 0) {
            return 0;
    }
    char query[255];

     while (1) 
    {
        scanf(" %[^\n]", query);
        send(sock, query, strlen(query), 0);

        bzero(buffer, 1024);
        read(sock, buffer, 1024);

        printf("%s\n", buffer);
    }

    return 0;

}
